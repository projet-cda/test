<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
$user = new User();
$user->loadUser($_SESSION['ID']);
$value = 0;
if (!empty($_GET['statut'])) {
    $value = intval($_GET['statut']);
}
$user->getAnnonces($value);
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tableau de bord</title>
</head>

<body>
    <?php
    require('header.php');
    if ($user->isBanned) {
    ?>
        <h2>Compte désactivé, veuillez contacté l'administrateur du site</h2>
    <?php
    } else {
    ?>
        <h2>Tableau de bord Utilisateur</h2>

        <p>Nombre d'annonces en cours : <?= $user->infos['en_cours'] ?></p>
        <p>Nombre d'annonces terminées dans le mois : <?= $user->infos['terminee'] ?></p>
        <p>CA réalisé : <?= $user->infos['CA'] ?>€</p>
        <button onclick="window.location='edit.php'">Ajouter une annonce</button>
        <form> Triez les annonces
            <select name="statut" onChange="submit(this)">
                <option value="0" <?php if ($value === 0) {
                                        echo "selected";
                                    } ?>>Tout</option>
                <option value="1" <?php if ($value === 1) {
                                        echo "selected";
                                    } ?>>En attente</option>
                <option value="2" <?php if ($value === 2) {
                                        echo "selected";
                                    } ?>>En cours</option>
                <option value="3" <?php if ($value === 3) {
                                        echo "selected";
                                    } ?>>Terminée</option>
            </select>
        </form>
        <div class="container">
            <table class="table table-hover table-primary">
                <thead class="thead-dark">
                    <tr>
                        <th>Titre</th>
                        <th>Prix</th>
                        <th>Modifier</th>
                        <th>Supprimer</th>
                        <th>Statut</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 0; $i < count($user->lesAnnonces); $i++) { ?>
                        <tr>
                            <td><?= $user->lesAnnonces[$i]->titre ?></td>
                            <td><?= $user->lesAnnonces[$i]->prix ?> €</td>
                            <td><button <?php if (!$user->lesAnnonces[$i]->isAchete) { ?> onclick="window.location='edit.php?idAnnonce=<?= $user->lesAnnonces[$i]->id ?>'" <?php } ?>>Modifier</button></td>
                            <td><button class="btn-danger" <?php if (!$user->lesAnnonces[$i]->isAchete) { ?> onClick="Javascript:if (confirm('Supprimer ?')){window.location='delete.php?idAnnonce=<?= $user->lesAnnonces[$i]->id ?>'}" <?php } ?>>Supprimer</button></td>
                            <td>
                                <?php
                                if ($user->lesAnnonces[$i]->isPublie == 1 && $user->lesAnnonces[$i]->isAchete == 0) {
                                    echo "En cours";
                                } elseif ($user->lesAnnonces[$i]->isPublie == 1 && $user->lesAnnonces[$i]->isAchete == 1) {
                                    echo "Terminée";
                                } else {
                                    echo "En attente";
                                }
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        </div>
</body>

</html>