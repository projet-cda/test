<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}

// Suppression de l'annonce
if (isset($_GET['idAnnonce'])) {
    $uneAnnonce = Annonce::construct_load($_GET['idAnnonce']);
    $uneAnnonce->photo->deletePhoto();
    $uneAnnonce->deleteAnnonce();
}

header("Location: panel.php");
die;
