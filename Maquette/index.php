<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Recupération des annnonces
$site = new Model();
$site->getAllAnnonces();
if (isset($_SESSION['ID'])) {
    if ($_SESSION['isAdmin']) {
        header("Location:admin.php");
    }
    else{
        $panier = new Panier();
        $panier->verifExistPanier();
    }
}
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accueil</title>
</head>

<body>
    <?php
    require('header.php');
    ?>
    <div class="container">
        <h1>Liste des annonces</h1><br>


        <?php
        for ($i = 0; $i < count($site->annonces); $i++) { ?>
            <div>
                <div class="card float-left" style="width: 18rem; margin:20px;">
                    <?php if (isset($site->annonces[$i]->photo->id)) { ?>
                        <img src="<?= $site->annonces[$i]->photo->url ?>" class="card-img-top" alt="Image annonce <?= $site->annonces[$i]->id ?>">
                    <?php } else { ?>
                        <img src="img/defaut.jpg" class="card-img-top" alt="image par defaut">
                    <?php } ?>
                    <div class="card-body">
                        <h5 class="card-title"><?= $site->annonces[$i]->titre ?></h5>
                        <p>Publiée le <?= $site->annonces[$i]->date->format("d/m/Y à H:i") ?></p>
                        <p class="card-text"><?= nl2br($site->annonces[$i]->description) ?></p>
                        <?= $site->annonces[$i]->prix ?> €<br>
                        <?php
                        if (isset($_SESSION['ID'])) {
                            if ($site->annonces[$i]->idClient !== $_SESSION['ID']) {
                                if ($panier->verifExistDansPanier($site->annonces[$i]->id)) { ?>
                                    <a class="btn btn-primary">Déjà dans le panier</a>
                                <?php } else { ?>
                                    <a href="ajoutPanier.php?idAnnonce=<?= $site->annonces[$i]->id ?>" class="btn btn-primary">Ajouter au panier</a>
                            <?php }
                            }
                        } else { ?>
                            <a href="login.php" class="btn btn-primary">Connectez-vous</a>
                        <?php }

                        ?>
                    </div>
                </div>
            <?php } ?>
            </div>
</body>

</html>