<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou pas admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || (!$_SESSION['isAdmin'])) {
    header("Location:index.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panneau Admin</title>
</head>

<body>
    <?php
    require('header.php');
    ?>

    <h2>Tableau de bord Admin</h2>

    <table>
        <thead>
            <tr>
                <th>Titre</th>
                <th>Valider</th>
                <th>Supprimer</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>--titre--</td>
                <td><button>Valider</button>
                <td><button>Supprimer</button>
                </td>
            </tr>
        </tbody>
    </table>

    <table>
        <thead>
            <tr>
                <th>Utilisateur</th>
                <th>Bannir</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>--nom-prenom--</td>
                <td><button>Bannir</button></td>
            </tr>
        </tbody>
    </table>
</body>

</html>