<?php
if (isset($_SESSION['ID'])) {
  $panierHeader = new Panier();
  $panierHeader->idClient = $_SESSION['ID'];
  $panierHeader->getAnnonces();
}
?>
<nav class="navbar navbar-expand-lg navbar-primary bg-dark">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <?php if (isset($_SESSION['ID'])) { ?>
        <?php if (($_SESSION['isAdmin'])) { ?>
          <li class="nav-item active">
            <a class="nav-link" href="admin.php">Accueil</a>
          </li>
        <?php } else { ?>
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Accueil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="panel.php">Tableau de bord</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="panier.php">Panier (<?= count($panierHeader->annonces) ?>)</a>
          </li>
        <?php } ?>
        <li class="nav-item">
          <a class="nav-link" href="logout.php">Déconnexion</a>
        </li>
      <?php } else { ?>
        <li class="nav-item active">
          <a class="nav-link" href="index.php">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="login.php">Connexion</a>
        </li>
      <?php } ?>
    </ul>
  </div>
</nav>