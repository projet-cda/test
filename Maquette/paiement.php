<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
// Recuperation infos utilisateur
$user = new User();
$user->loadUser($_SESSION['ID']);
if (!$user->rue || !$user->cp || !$user->ville) {
    header("Location:livraison.php");
}

// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paiement</title>
</head>

<body>
    <?php
    require('header.php');
    ?>

    <ul class="nav nav-tabs justify-content-center">
        <li class="nav-item">
            <a class="nav-link" href="panier.php">Panier</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="livraison.php">Livraison</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active">Paiement</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Confirmation</a>
        </li>
    </ul>

    <!-- Paiement -->
    <div>
        <h1>Paiement</h1>
        <p>Réference paiement : 400224208500</p>
        <p>Mode de paiement : Visa</p>
        <p>Montant : EUR 32.95</p>
        <form method="POST" action="success.php">
            <p>Numero de la carte <input type="text"></p>
            <p>Date d'expiration
                <select>
                    <option>--</option>
                    <?php for ($i = 0; $i <= 31; $i++) {
                        echo "<option>$i</option>";
                    } ?>
                </select>
                <select>
                    <option>----</option>
                    <?php for ($i = 2020; $i <= 2022; $i++) {
                        echo "<option>$i</option>";
                    } ?>
                </select></p>
            <p>Titulaire de la carte <input type="text"></p>
            <p>Code de sécurité <input type="text"></p>
            <input type="submit" value="Effectuer le paiement">
        </form>
    </div>
    <!-- Mon panier -->
    <div>
        <h4>Mon panier</h4>
        <p><img>Description<br>Prix</p>
        <p><img>Description<br>Prix</p>
        <p><img>Description<br>Prix</p>
        <!-- Recapitulatif -->
        <div>
            <h4>Récapitulatif de votre commande</h4>
            Sous-total de vos achat : 26,00€<br>
            Reductions : 0.00€<br>
            Livraison : 6.95€<br>
            <p>Montant Total 32.95€</p>
        </div>
        <!-- Fin recapitulatif -->
    </div>
    <!-- Fin panier -->
</body>

</html>