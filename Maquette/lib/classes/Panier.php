<?php

class Panier extends Model{

    protected $id; 
    protected $isValide;
    protected $idClient;
    protected $annonces;

    function __construct()
    {
        parent::__construct();
    }

    /*
    * Récupère toutes les annonces du panier
    */
    function getAnnonces()
    {
        $resultats = array();
        $req = "SELECT annonce.id AS idAnnonce, panier.id AS idPanier FROM annonce,annoncepanier, panier";
        $req .= " WHERE annonce.id=annoncepanier.idAnnonce AND annoncepanier.idPanier=panier.id and panier.idClient=:id AND annonce.isAchete=0";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([":id"=>$this->idClient]);
        $resultats = $stmt->fetchAll(PDO::FETCH_ASSOC);
        for ($i=0; $i < count($resultats); $i++) { 
            $this->annonces[] = Annonce::construct_load($resultats[$i]['idAnnonce']);
            $this->id = $resultats[$i]['idPanier'];
        }
    }

    /*
    * Suppression d'une annonce dans le panier
    */
    function deleteAnnoncePanier($idAnnonce,$idPanier){
        $req = "DELETE FROM annoncepanier WHERE idAnnonce=:idAnnonce AND idPanier=:idPanier";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":idAnnonce"    => $idAnnonce,
            ":idPanier"     => $idPanier
        ]);
    }

    /*
    * Cloture le panier, isValid passe à 1, et toutes les annonces du panier passent à isAchete=1
    */
    function cloturerPanier(){
        $req = "SELECT annonce.id AS idAnnonce, panier.id AS idPanier FROM annonce, annoncepanier, panier WHERE annonce.id=annoncepanier.idAnnonce";
        $req .=" AND annoncepanier.idPanier=panier.id AND panier.id=:id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([":id"=>$this->id]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        for ($i=0; $i < count($result); $i++) { 
            $stmt2 = $this->pdo->prepare("UPDATE annonce SET isAchete=1 WHERE id=:id");
            $stmt2->execute([":id"=>$result[$i]['idAnnonce']]);
            $stmt3 = $this->pdo->prepare("DELETE FROM annoncepanier WHERE idAnnonce=:idAnnonce AND idPanier!=:idPanier");
            $stmt3->execute([
                ":idAnnonce"    =>$result[$i]['idAnnonce'],
                ":idPanier"     =>$result[$i]['idPanier']
                ]);
        }
        $req = "UPDATE panier SET isValide=1 WHERE idClient=:id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([":id"=>$_SESSION['ID']]);


    }

    /*
    * Verifie si un panier existe dejà, si non, il le crée
    */
    function verifExistPanier(){
        $stmt = $this->pdo->prepare("SELECT id FROM panier WHERE idClient=:idClient AND isValide=0");
        $stmt->execute([":idClient"=>$_SESSION['ID']]);
        if($stmt->RowCount()){
            $resultat = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->id = $resultat['id'];
        }
        else{
            $req = "INSERT INTO panier (idClient) VALUES (:idClient)";
            $stmt = $this->pdo->prepare($req);
            $stmt->execute([":idClient"=>$_SESSION['ID']]);
            $this->id = $this->pdo->lastInsertId();
        }
    }

    /*
    * Ajout une annonce dans le panier
    */
    function  ajoutPanier($idAnnonce){
        $req = "INSERT INTO annoncepanier (idPanier, idAnnonce) VALUES(:idPanier, :idAnnonce)";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":idPanier"     =>$this->id,
            ":idAnnonce"    => $idAnnonce

        ]);
    }

    /*
    * Verifie si une annonce est dejà dans le panier
    */
    function verifExistDansPanier($idAnnonce){
        $req = "SELECT * FROM annoncepanier WHERE idPanier=:idPanier AND idAnnonce=:idAnnonce";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":idPanier"     =>$this->id,
            ":idAnnonce"    => $idAnnonce
        ]);
        return $stmt->RowCount();
    }

    function __get($name)
    {
        return $this->$name;
    }

    function __set($name, $value)
    {
        $this->$name = $value;
    }
}