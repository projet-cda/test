<?php

class Model
{

    protected $pdo;
    protected $annonces;

    public function __construct()
    {
        $this->pdo = Database::getPdo();
    }

    /*
    * Retourne toutes les annonces publiées et non achetées
    */
    public function getAllAnnonces()
    {
        $resultats = array();
        $requete = "SELECT id FROM annonce WHERE isPublie=1 AND isAchete=0 ORDER BY date DESC";
        $stmt = $this->pdo->prepare($requete);
        $stmt->execute();
        $resultats = $stmt->fetchAll();

        for ($i = 0; $i < count($resultats); $i++) {
            $this->annonces[] = Annonce::construct_load($resultats[$i]['id']);
        }
    }

    // GETTER
    function __get($name)
    {
        return $this->$name;
    }
}
