<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
// Recupération infos sur l'utilisateur
$user = new User();
$user->loadUser($_SESSION['ID']);
// Si l'utilisateur est banni, renvoie vers panel
if ($user->isBanned) {
    header("Location:panel.php");
}

$annonce = new Annonce();
$erreur = null;

// Gestion du post
if (!empty($_POST)) {

    $idAnnonce = null;
    if (!empty($_POST['idAnnonce'])) {
        $idAnnonce = intval($_POST['idAnnonce']);
    }

    $titre = null;
    if (!empty($_POST['titre'])) {
        $titre = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['titre']))))));
    }

    $prix = null;
    if (!empty($_POST['prix'])) {
        $prix = floatval($_POST['prix']);
    }

    $description = null;
    if (!empty($_POST['description'])) {
        $description = str_replace(array("<script>,</script>"), array("", ""), $_POST['description']);
    }

    if ($titre && $description && $prix) {
        $annonce->id = $idAnnonce;
        $annonce->titre = $titre;
        $annonce->prix = $prix;
        $annonce->description = $description;
        $annonce->save();

        if (!empty($_FILES["fileToUpload"]['name'])) {
            $target_dir = "img/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

            $_FILES["fileToUpload"]["name"] = $annonce->id . "." . $imageFileType;
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

            move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);

            $annonce->photo = new Photo();
            $annonce->photo->url = $target_file;

            $annonce->photo->upload($annonce->id);
        }

        header("Location:panel.php");
    } else {
        $erreur = "Echec de la sauvegarde";
    }
}

// Recupération de l'annonce en mode edition
if (isset($_GET['idAnnonce'])) {
    $annonce->load(intval($_GET['idAnnonce']));
}
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <script src="lib/js/jquery-3.5.1.min.js"></script>
    <script src="lib/js/jquery.validate.js" referrerpolicy="origin"></script>
    <script src="lib/js/tinymce/tinymce.min.js"></script>
    <script>
        $(function() {
            tinymce.init({
                selector: 'textarea',
                invalid_elements: 'script',
                width: 600,
                height: 400,
            });
        });
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tableau de bord - Ajout/Modification d'une annonce</title>
</head>

<body>
    <?php
    require('header.php');
    ?>
    <h2>Tableau de bord Utilisateur</h2>
    <?php
    if ($erreur) {
        echo "$erreur";
    }
    ?>
    <form action="#" method="POST" enctype="multipart/form-data">
        <label for="titre">Titre : </label>
        <input type="text" name="titre" value="<?= $annonce->titre ?>" required><br>
        <label for="prix">Prix : </label>
        <input type="number" step="0.01" min="0" name="prix" value="<?= $annonce->prix ?>" required><br>
        <label for="description">Description</label><br>
        <textarea name="description"><?= $annonce->description ?></textarea>
        <!-- Module photo -->
        <label for="fileToUpload">Image : </label><br>
        <input type="file" name="fileToUpload" id="fileToUpload"><br>
        <input type="submit" value="Valider">
        <input type="hidden" name="idAnnonce" value="<?= $annonce->id ?>">
    </form>
</body>

</html>