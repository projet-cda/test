<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
// Infos user
$panier = new Panier();
$panier->idClient = $_SESSION['ID'];
$panier->getAnnonces();
$panier->cloturerPanier();

// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirmation</title>
</head>

<body>
    <?php
    require('header.php');
    ?>

<ul class="nav nav-tabs justify-content-center">
        <li class="nav-item">
            <a class="nav-link">Panier</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Livraison</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Paiement</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active">Confirmation</a>
        </li>
    </ul>


    Nous vous remercions blablabla
    <!-- Mon panier -->
    <div>
        <h4>Mon panier</h4>
        <p><img>Description<br>Prix</p>
        <p><img>Description<br>Prix</p>
        <p><img>Description<br>Prix</p>
        <!-- Recapitulatif -->
        <div>
            <h4>Récapitulatif de votre commande</h4>
            Sous-total de vos achat : 26,00€<br>
            Reductions : 0.00€<br>
            Livraison : 6.95€<br>
            <p>Montant Total 32.95€</p>
        </div>
        <!-- Fin recapitulatif -->
    </div>
    <!-- Fin panier -->