<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
$panier = new Panier();
$panier->idClient = $_SESSION['ID'];
$panier->getAnnonces();
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
</head>

<body>
    <?php
    require('header.php');
    ?>
    <ul class="nav nav-tabs justify-content-center">
        <li class="nav-item">
            <a class="nav-link active" href="#">Panier</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Livraison</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Paiement</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Confirmation</a>
        </li>
    </ul>

    <!-- Panier -->
    <div>
        <h1>Panier</h1>
        <?php
        for ($i = 0; $i < count($panier->annonces); $i++) { ?>
            <h2><?= $panier->annonces[$i]->titre ?></h2>
            <p><?= $panier->annonces[$i]->description ?></p>
            <p><?= $panier->annonces[$i]->prix ?> €</p>
            <button onclick="Javascript:if (confirm('Supprimer ?')){window.location='deleteFromPanier.php?idAnnonce=<?= $panier->annonces[$i]->id ?>&idPanier=<?= $panier->id ?>'}">Supprimer du panier</button>
        <?php } ?>

        <form action="livraison.php">
            <input type="submit" value="Valider le panier">
        </form>
    </div>

    <!-- Recapitulatif -->
    <div>
        <h4>Récapitulatif de votre commande</h4>
        Sous-total de vos achat : 26,00€<br>
        Reductions : 0.00€<br>
        Livraison : 6.95€<br>
        <p>Montant Total 32.95€</p>
    </div>
    <!-- Fin recapitulatif -->
</body>

</html>