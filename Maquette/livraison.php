<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
// Infos user
$user = new User();
$user->loadUser($_SESSION['ID']);

// Traitement du post
$erreur = null;
if (!empty($_POST)) {
    $tel = null;
    if (!empty($_POST['tel'])) {
        $user->telephone = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['tel']))))));
    }
    $cp = null;
    if (!empty($_POST['cp'])) {
        $user->cp = intval($_POST['cp']);
    }
    $rue = null;
    if (!empty($_POST['rue'])) {
        $user->rue = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['rue']))))));
    }
    $ville = null;
    if (!empty($_POST['ville'])) {
        $user->ville = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['ville']))))));
    }
    $user->id = $_SESSION['ID'];
    $user->save();
    header("Location:livraison.php");
}
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <script src="lib/js/jquery-3.5.1.min.js"></script>
    <script src="lib/js/jquery.validate.js" referrerpolicy="origin"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Livraison</title>
</head>

<body>
    <?php
    require('header.php');
    ?>

    <ul class="nav nav-tabs justify-content-center">
        <li class="nav-item">
            <a class="nav-link" href="panier.php">Panier</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active">Livraison</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Paiement</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Confirmation</a>
        </li>
    </ul>

    <!-- Formulaire -->
    <div>
        <h1>Livraison</h1>
        <form action="#" method="POST">
            <label for="nom">Nom </label>
            <input type="text" name="nom" placeholder="nom" value="<?= $user->nom ?>" disabled><br>
            <label for="rue">Prenom</label>
            <input type="text" name="prenom" placeholder="prenom" value="<?= $user->prenom ?>" disabled><br>
            <label for="tel">Telephone</label>
            <input type="text" name="tel" placeholder="telephone" value="<?= $user->telephone ?>" required><br>
            <label for="rue">Rue</label>
            <input type="text" name="rue" placeholder="rue" value="<?= $user->rue ?>" required><br>
            <label for="cp">Code postal</label>
            <input type="number" name="cp" placeholder="code postal" value="<?= $user->cp ?>" required><br>
            <label for="ville">Ville</label>
            <input type="text" name="ville" placeholder="ville" value="<?= $user->ville ?>" required><br>
            <input type="submit" value="Valider adresse">
        </form><br>

        <form action="paiement.php">
            <input type="submit" value="Procéder au paiement">
        </form>
    </div>
    <!-- Mon panier -->
    <div>
        <h4>Mon panier</h4>
        <p><img>Description<br>Prix</p>
        <p><img>Description<br>Prix</p>
        <p><img>Description<br>Prix</p>
        <!-- Recapitulatif -->
        <div>
            <h4>Récapitulatif de votre commande</h4>
            Sous-total de vos achat : 26,00€<br>
            Reductions : 0.00€<br>
            Livraison : 6.95€<br>
            <p>Montant Total 32.95€</p>
        </div>
        <!-- Fin recapitulatif -->
    </div>
    <!-- Fin panier -->
</body>

</html>